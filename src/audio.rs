use std::sync::mpsc::Receiver;
use std::thread;
use cpal::SampleFormat;
use cpal::traits::{DeviceTrait, HostTrait, StreamTrait};

pub fn audio_thread(enabled: bool, audio_rx: Receiver<Vec<f32>>, av_sync_rx: Receiver<u8>, finish: Receiver<u8>) {
    thread::spawn(move || {
        if !enabled {
            // just consume the sync signals to unblock and return
            av_sync_rx.recv().unwrap();
            finish.recv().unwrap();
            return;
        }

        let host = cpal::default_host();
        let device = host.default_output_device().expect("audio output device must be present");

        let sample_rate = cpal::SampleRate(48000);

        let mut supported_configs = device.supported_output_configs().unwrap();
        let config = supported_configs.find(|config| {
            return config.sample_format() == SampleFormat::F32 &&
                config.channels() == 2 &&
                config.min_sample_rate() <= sample_rate &&
                config.max_sample_rate() >= sample_rate;
        }).expect("no compatible audio device found")
            .with_sample_rate(sample_rate).config();

        let mut buffer: Vec<f32> = audio_rx.recv().unwrap();
        let mut idx = 0;

        av_sync_rx.recv().unwrap();

        let result = device.build_output_stream(
            &config,
            move |data: &mut [f32], _: &cpal::OutputCallbackInfo| {
                // println!("audio: {} {} {}", buffer.len(), idx, data.len());
                for sample in data.iter_mut() {
                    if idx >= buffer.len() {
                        buffer = audio_rx.recv().unwrap();
                        idx = 0;
                    }
                    *sample = buffer[idx];
                    idx += 1;
                }
            },
            |err| {
                panic!("error during audio playback: {:?}", err);
            },
            None
        ).unwrap();

        result.play().unwrap();

        // wait until video is done, so that the stream object doesn't get discarded
        finish.recv().unwrap();
    });
}
