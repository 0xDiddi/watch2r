use std::io::{BufWriter, stdout, Write};
use std::ops::Add;
use std::sync::mpsc::{Receiver, SyncSender};
use std::thread;
use std::thread::{JoinHandle, sleep};
use std::time::{Duration, Instant};

pub fn video_thread(out_w: u32, out_h: u32, out_rate: f64,
                    video_rx: Receiver<Vec<u8>>, stride_rx: Receiver<usize>,
                    av_sync_tx: SyncSender<u8>,
                    timing_bell: bool, color_threshold: f32) -> JoinHandle<()> {
    return thread::spawn(move || {
        sleep(Duration::from_millis(5000));

        let output: Box<dyn Write> = Box::new(stdout().lock());

        let mut writer = BufWriter::new(output);

        write!(writer, "{}[H{}[J", 27 as char, 27 as char).unwrap();
        writer.flush().unwrap();

        let stride = stride_rx.recv().unwrap();

        let frame_time = Duration::from_secs_f64(1.0 / out_rate);
        let mut frame_finish = Instant::now();

        let mut skip = false;
        let mut prev_skip = false;

        av_sync_tx.send(0).unwrap();

        for data in video_rx {
            // zero-length vec signals we've reached the end
            if data.len() == 0 {
                break;
            }

            if skip {
                frame_finish = frame_finish.add(frame_time);
                skip = false;
                continue;
            }

            write!(writer, "{}[H", 27 as char).unwrap();

            for y in (0..out_h).step_by(2) {
                let line_upper = (y as usize) * stride;
                let line_lower = ((y + 1) as usize) * stride;

                let mut prev_color_top: (u8, u8, u8) = (0, 0, 0);
                let mut prev_color_bot: (u8, u8, u8) = (0, 0, 0);

                for x in 0..out_w {
                    let col_upper = line_upper + ((x as usize) * 3);
                    let col_lower = line_lower + ((x as usize) * 3);

                    let color_top = (data[col_upper], data[col_upper + 1], data[col_upper + 2]);
                    let mut color_bot = (0, 0, 0);

                    if y < (out_h - 1) {
                        color_bot = (data[col_lower], data[col_lower + 1], data[col_lower + 2]);
                    }

                    let dist_top = prev_color_top.distance(color_top) > color_threshold;
                    let dist_bot = prev_color_bot.distance(color_bot) > color_threshold;

                    // only change stored previous color when it is being printed too
                    if dist_top { prev_color_top = color_top };
                    if dist_bot { prev_color_bot = color_bot };

                    match (dist_top, dist_bot) {
                        (true, true) => write!(writer, "\x1b[38;2;{};{};{}m\x1b[48;2;{};{};{}m▀",
                                               color_top.0, color_top.1, color_top.2,
                                               color_bot.0, color_bot.1, color_bot.2).unwrap(),
                        (true, false) => write!(writer, "\x1b[38;2;{};{};{}m▀",
                                                color_top.0, color_top.1, color_top.2).unwrap(),
                        (false, true) => write!(writer, "\x1b[48;2;{};{};{}m▀",
                                                color_bot.0, color_bot.1, color_bot.2).unwrap(),
                        (false, false) => write!(writer, "▀").unwrap(),
                    };
                }
                writer.write("\n".as_bytes()).unwrap();
            }

            /*
            // older algorithm using full blocks and no distance function
            for y in 0..out_h {
                let line_offset = (y as usize) * stride;
                for x in 0..out_w {
                    let col_offset = line_offset + ((x as usize) * 3);
                    write!(writer,
                           "\x1b[38;2;{};{};{}m██",
                           data[col_offset],
                           data[col_offset + 1],
                           data[col_offset + 2],
                    ).unwrap();
                }
                writer.write("\n".as_bytes()).unwrap();
            }
            */

            writer.flush().unwrap();

            frame_finish = frame_finish.add(frame_time);
            let now = Instant::now();
            let sleep_time = frame_finish.checked_duration_since(now);

            match sleep_time {
                Some(time) => {
                    sleep(time);
                    prev_skip = false;
                }
                _ => {
                    if timing_bell && prev_skip {
                        writer.write(&[0x7 as u8]).unwrap();
                    }

                    skip = true;
                    prev_skip = true;
                }
            }
        }
    });
}

trait Dist {
    fn distance(&self, other: (u8, u8, u8)) -> f32;
}

impl Dist for (u8, u8, u8) {
    fn distance(&self, other: (u8, u8, u8)) -> f32 {
        let d1 = (self.0 as f32) - (other.0 as f32);
        let d2 = (self.1 as f32) - (other.1 as f32);
        let d3 = (self.2 as f32) - (other.2 as f32);

        return ((d1 * d1) + (d2 * d2) + (d3 * d3)).sqrt();
    }
}
