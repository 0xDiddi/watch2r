mod video;
mod audio;
mod parameters;

use std::ptr;
use std::sync::mpsc::{channel, sync_channel};
use ffmpeg_next::{ChannelLayout, Error, format, Format};
use ffmpeg_next::codec::Context as CodecContext;
use ffmpeg_next::codec::packet::Packet;
use ffmpeg_next::decoder::Audio;
use ffmpeg_next::format::{Pixel, Sample};
use ffmpeg_next::format::sample::Type as SampleType;
use ffmpeg_next::media::Type;
use ffmpeg_next::util::frame::Video as VFrame;
use ffmpeg_next::util::frame::Audio as AFrame;
use ffmpeg_next::software::scaling::{Context as SwsContext, Flags};
use ffmpeg_next::software::resampling::Context as SwrContext;

use crate::audio::audio_thread;
use crate::video::video_thread;
use crate::parameters::{parameters, Resolution};

fn main() {
    let params = parameters().run();

    let (out_w, out_h) = match params.resolution {
        Resolution::Preset { preset } => preset,
        Resolution::WidthHeight { width, height } => (width, height),
    };

    let out_rate = params.frame_rate;

    let audio_enabled = !params.audio_disabled;

    unsafe {
        let mut input = format::open(
            &params.file.as_path(),
            &Format::Input(format::Input::wrap(ptr::null_mut())),
        ).unwrap().input();

        let video_str = input.streams().best(Type::Video).expect("file must have a video stream");
        let video_idx = video_str.index();

        let mut video_codec = CodecContext::from_parameters(video_str.parameters()).unwrap()
            .decoder().video().unwrap();

        let mut audio_idx: usize = 0;
        let mut audio_codec: Option<Audio> = None;
        if audio_enabled {
            let audio_str = input.streams().best(Type::Audio).unwrap();
            audio_idx = audio_str.index();
            audio_codec = Some(CodecContext::from_parameters(audio_str.parameters()).unwrap()
                .decoder().audio().unwrap());
        }

        let mut packet = Packet::new(32768);
        let mut video_frame = VFrame::empty();
        let mut audio_frame = AFrame::empty();

        let mut scaler = SwsContext::get(
            video_codec.format(),
            video_codec.width(),
            video_codec.height(),
            Pixel::RGB24,
            out_w,
            out_h,
            Flags::BICUBIC,
        ).expect("software scaler should be available");

        let mut resampler: Option<SwrContext> = None;
        if audio_enabled {
            let codec = audio_codec.as_ref().unwrap();
            resampler = Some(SwrContext::get(
                codec.format(),
                codec.channel_layout(),
                codec.rate(),
                Sample::F32(SampleType::Packed),
                ChannelLayout::STEREO,
                48000).expect("audio resample should be available"));
        }

        // frame-skip as detailed here: https://stackoverflow.com/a/2801744
        let rate = video_str.rate();
        let fps = (rate.numerator() as f64) / (rate.denominator() as f64);
        if out_rate > fps {
            panic!("target fps ({}) must be smaller than input fps ({})", out_rate, fps)
        }

        let skip_dt = fps / out_rate;
        // init as -1 so that we can increment first even on the first frame
        let mut frame_num = -1;
        let mut skip_t: f64 = 0.0;

        let (video_tx, video_rx) = channel::<Vec<u8>>();
        let (audio_tx, audio_rx) = channel::<Vec<f32>>();

        let (av_sync_tx, av_sync_rx) = sync_channel::<u8>(0);
        let (finish_tx, finish_rx) = sync_channel::<u8>(0);

        // this is used to send the stride (buffer width per line), as it is not always equal to the video width
        let (stride_tx, stride_rx) = channel();
        let mut stride_sent = false;

        let video_thread = video_thread(out_w, out_h, out_rate,
                                        video_rx, stride_rx, av_sync_tx,
                                        params.timing_bell, params.color_threshold);
        audio_thread(audio_enabled, audio_rx, av_sync_rx, finish_rx);

        let mut eof = false;

        loop {
            'read: loop {
                let res = packet.read(&mut input);
                match res {
                    Err(Error::Eof) => {
                        let res = video_codec.send_eof();
                        match res {
                            Err(Error::Eof) => {}
                            _ => res.unwrap()
                        }

                        let res = audio_codec.as_mut().unwrap().send_eof();
                        match res {
                            Err(Error::Eof) => {}
                            _ => res.unwrap()
                        }

                        eof = true;
                        break 'read;
                    }
                    Err(err) => {
                        panic!("packet read error: {:?}", err);
                    }
                    _ => {
                        let stream_idx = packet.stream();
                        if stream_idx == video_idx {
                            video_codec.send_packet(&packet).unwrap();
                            break 'read;
                        } else if audio_enabled && stream_idx == audio_idx {
                            audio_codec.as_mut().unwrap().send_packet(&packet).unwrap();
                            break 'read;
                        }
                    }
                }
            }

            'video_receive: loop {
                let err = video_codec.receive_frame(&mut video_frame).err();
                match err {
                    // EAgain - awaiting more data
                    Some(Error::Other { errno }) if errno == 35 => {
                        break 'video_receive;
                    }
                    Some(Error::Eof) => {
                        break 'video_receive;
                    }
                    Some(err) => {
                        panic!("rec err: {:?}", err);
                    }
                    _ => {}
                }

                frame_num += 1;
                if frame_num != (skip_t.floor() as i32) {
                    continue 'video_receive;
                }
                skip_t += skip_dt;

                let mut out = VFrame::empty();
                out.alloc(Pixel::RGB24, out_w, out_h);

                scaler.run(&video_frame, &mut out).unwrap();

                let data = out.data(0).to_owned();

                if !stride_sent {
                    stride_sent = true;
                    stride_tx.send(out.stride(0)).unwrap();
                }

                video_tx.send(data).unwrap();
            }

            if !audio_enabled {
                continue;
            }

            'audio_receive: loop {
                let err = audio_codec.as_mut().unwrap().receive_frame(&mut audio_frame).err();
                match err {
                    // EAgain - awaiting more data
                    Some(Error::Other { errno }) if errno == 35 => {
                        break 'audio_receive;
                    }
                    Some(Error::Eof) => {
                        break 'audio_receive;
                    }
                    Some(err) => {
                        panic!("rec err: {:?}", err);
                    }
                    _ => {}
                }

                let mut out = AFrame::empty();
                let d = resampler.as_ref().unwrap().output();
                let rate = d.rate as usize;
                out.alloc(d.format, rate, d.channel_layout);

                resampler.as_mut().unwrap().run(&audio_frame, &mut out).unwrap();

                let data_semi_planar = out.plane::<(f32, f32)>(0).to_owned();

                // these two lines convert a vec of tuples to a straight vec
                let mut data_packed: Vec<f32> = std::mem::transmute(data_semi_planar);
                data_packed.set_len(data_packed.len() * 2);

                audio_tx.send(data_packed).unwrap();
            }

            if eof {
                break;
            }
        }

        // send zero-length vec to signal EOF
        video_tx.send(Vec::new()).unwrap();
        video_thread.join().unwrap();
        finish_tx.send(0).unwrap();
    }
}
