use std::path::PathBuf;
use bpaf::{Bpaf};

#[derive(Debug, Bpaf)]
#[bpaf(options)]
pub struct Parameters {
    #[bpaf(external)]
    pub resolution: Resolution,

    /// Specifies the output frame rate.
    /// Must be lower than the source frame rate.
    /// Default 25 fps.
    #[bpaf(short, long, env("WATCH2R_RATE"), argument("FPS"), fallback(25.0))]
    pub frame_rate: f64,

    /// Whether audio playback is disabled. Default is audio enabled.
    #[bpaf(short, long, env("WATCH2R_AUDIO"))]
    pub audio_disabled: bool,

    /// Whether to ring the terminal bell when video timing is off.
    #[bpaf(short, long, hide)]
    pub timing_bell: bool,

    /// Threshold (in vector distance) below which no new ansi color code will be emitted.
    #[bpaf(short, long, hide, fallback(5.0))]
    pub color_threshold: f32,

    /// The name of the file to play.
    #[bpaf(positional("FILE"))]
    pub file: PathBuf,
}

#[derive(Debug, Bpaf)]
pub enum Resolution {
    Preset {
        /// Specifies the playback resolution as a 0-based index into an array of presets.
        /// The preset values are:
        /// 480x270, 400x225, 384x216, 368x207, 352x198, 336x189, 320x180, 304x171, 288x162, 272x153, 256x144, 240x135, 121x68, 80x45, 64x36.
        ///
        /// Default 7 (304x171).
        #[bpaf(short, long, argument::<usize>("IDX"), fallback(7))]
        #[bpaf(guard(check_preset, "preset index out of range"), map(select_preset))]
        preset: (u32, u32),
    },
    WidthHeight {
        /// Specifies playback resolution width in pixels
        #[bpaf(short, long, env("WATCH2R_WIDTH"), argument("PIX"))]
        width: u32,
        /// Specifies playback resolution height in pixels
        #[bpaf(short, long, env("WATCH2R_HEIGHT"), argument("PIX"))]
        height: u32,
    }
}

const PRESET_RESOLUTIONS: [&'static(u32, u32); 15] = [
    &(480, 270),
    &(400, 225), // audio desync
    &(384, 216), // occasional stutters
    &(368, 207), // tearing starting to become noticeable
    &(352, 198),
    &(336, 189),
    &(320, 180),
    &(304, 171),
    &(288, 162),
    &(272, 153),
    &(256, 144),
    &(240, 135),
    &(121, 68),
    &(80, 45),
    &(64, 36),
];

fn check_preset(n: &usize) -> bool {
    *n < PRESET_RESOLUTIONS.len()
}

fn select_preset(n: usize) -> (u32, u32) {
    *PRESET_RESOLUTIONS[n]
}
